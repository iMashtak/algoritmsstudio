﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Editor {
	class ComparedPair<TKey, TValue> : IComparable where TKey : IComparable {
		public TKey Key { get; private set; }
		public TValue Value { get; private set; }

		public ComparedPair(TKey key, TValue item) {
			Key = key;
			Value = item;
		}

		public int CompareTo(object obj) {
			ComparedPair<TKey, TValue> pair = (ComparedPair<TKey, TValue>)obj;
			return Key.CompareTo(pair.Key);
		}

		public override string ToString() {
			return $"({Key}:{Value})";
		}

	}
}

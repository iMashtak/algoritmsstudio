﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Editor
{
	class Pair<TKey, TValue> {
		public TKey Key { get; set; }
		public TValue Value { get; set; }

		public Pair(TKey key, TValue item) {
			Key = key;
			Value = item;
		}

		public override bool Equals(object obj) {
			return Key.Equals(obj);
		}

		public override int GetHashCode()
		{
			return Key.GetHashCode();
		}

		public override string ToString() {
			return $"({Key}:{Value})";
		}

	}
}

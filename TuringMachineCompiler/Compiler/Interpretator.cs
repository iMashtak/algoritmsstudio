﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace TuringMachineCompiler.Compiler
{

    public class Interpretator
    {

		public enum Mode {
			Debug, Release
		}

		public delegate void PrintInformation(string message);

		public ErrorsHandler ErrorsHandler {
			get;
		}

		public Mode CurrentMode {
			get;
		}

		private Builder Builder {
			get;
		}

		public PrintInformation Printer {
			get;set;
		}

		public Interpretator(Mode mode)
		{
			Builder = new Builder();
			ErrorsHandler = new ErrorsHandler();
			CurrentMode = mode;
			Printer = delegate (string message)
			{
				Console.WriteLine(message);
			};
		}

		public bool Interpretate(string input, string codePath, out string result)
		{

			string MashineTape_ToString(List<char> tape)
			{
				string res = "";
				foreach (char symbol in tape) {
					if (symbol == '\\' || symbol == '/') {
						continue;
					}
					res += symbol;
				}
				return res;
			}

			result = null;
			if (Builder.Build(codePath, out string[] codeLines) == false) {
				ErrorsHandler.Add(Builder.ErrorsHandler);
				return false;
			}
			string currentState = $"{Builder.MainNamespace}_Start";
			int symbolNumber = 0;
			List<char> mashineTape = input.ToList();
			mashineTape.Insert(0, '/');
			mashineTape.Add('\\');
			int localCounter = 0;
			bool endOfWork = false;
			if (CurrentMode == Mode.Debug) {
				Printer(
					$"Шаг: {localCounter}, позиция {symbolNumber + 1}\n" +
					$"    Строка: <nothing>\n" +
					$"        {MashineTape_ToString(mashineTape)}\n");
			}
			while (endOfWork == false) {
				endOfWork = true;
				foreach (var line in codeLines) {
					string[] parts = line.Split(' ');
					if (parts[0] != currentState) {
						continue;
					}
					if (parts[1].Contains($"{mashineTape[symbolNumber]}") == false) {
						continue;
					}
					string currentSymbol = parts[1];
					string newSymbol = parts[3];
					string path = parts[4];
					string newState = parts[5];
					for (int i = 0; i < newSymbol.Length; ++i) {
						if (mashineTape[symbolNumber] == currentSymbol[i]) {
							mashineTape[symbolNumber] = newSymbol[i];
						}
					}
					switch (path) {
						case "L": {
							--symbolNumber;
							if (symbolNumber < 0) {
								ErrorsHandler.NewIntrepretatorError("Уход за границу ленты");
								return false;
							}
							break;
						}
						case "H": {
							break;
						}
						case "R": {
							++symbolNumber;
							break;
						}
						default: {
							ErrorsHandler.NewIntrepretatorError("Невозможная область. Если Вы получили это сообщение, Вы хакнули синтаксический анализатор");
							return false;
						}
					}
					if (symbolNumber == mashineTape.Count - 1) {
						mashineTape.Add('\\');
					}
					currentState = newState;
					endOfWork = false;
					++localCounter;
					if (CurrentMode == Mode.Debug) {
						Printer(
							$"Шаг: {localCounter}, позиция {symbolNumber + 1}\n" +
							$"    Строка: {line}\n" +
							$"        {MashineTape_ToString(mashineTape)}\n");
					}
					break;
				}
			}
			result = MashineTape_ToString(mashineTape);
			return true;
		}

	}
}

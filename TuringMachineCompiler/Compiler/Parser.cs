﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TuringMachineCompiler.Compiler
{
    static class Parser
    {
		public static List<string> Parse(string code)
		{
			char getLastSymbol(string str)
			{
				return str[str.Length - 1];
			}
			string[] parsedCode = code.Split("\r\n");
			List<string> listCode = parsedCode.ToList();
			for (int i = 0; i < listCode.Count; ++i) {
				if (listCode[i] == "") {
					listCode.Remove(listCode[i]);
					--i;
				}
			}
			return listCode;
		}
	}
}

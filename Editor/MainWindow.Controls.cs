﻿using System;
using System.Collections.Generic;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Collections;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using AvaloniaEdit;
using System.IO;
using Avalonia.Input;
using AvaloniaEdit.Highlighting;

namespace Editor {

	public partial class MainWindow {
		private TextEditor _textEditor;
		private TextBox _outputBox;
		private TextBox _inputBox;
		private Button _runButton;
		private Button _stopButton;
		private Button _continueButton;
		private TextBlock _compiledBox;
		private TextBlock _errorsBox;
		private DefaultDialogService _dialogService;
		private MenuItem _newMenuItem;
		private MenuItem _openMenuItem;
		private MenuItem _saveMenuItem;
		private MenuItem _saveAsMenuItem;
		private ListBox _listOfFiles;
		private TextBlock _helpBox;
		private Button _helpCloseButton;
		private MenuItem _helpMenuItem;
		private DockPanel _mainDockPanel;
		private DockPanel _helpDockPanel;
		private List<Pair<string, bool>> _currentFiles;
		private Pair<string, bool> _currentFile;
		private AvaloniaList<ListBoxItem> _listOfFilesItems;
		private Menu _mainMenu;

		private void InitializeComponent() {
			var theme = new Avalonia.Themes.Default.DefaultTheme();
			theme.FindResource("Button");
			AvaloniaXamlLoader.Load(this);

			_currentFiles = new List<Pair<string, bool>>();
			_listOfFilesItems = new AvaloniaList<ListBoxItem>();

			_textEditor = this.FindControl<TextEditor>("Editor");
			_textEditor.Background = Brushes.LightGray;
			_textEditor.ShowLineNumbers = true;
			_textEditor.Options.HighlightCurrentLine = true;
			_textEditor.TextChanged += _textEditor_TextChanged;
			_textEditor.Text = "";
			_textEditor.TextArea.KeyDown += textEditor_TextArea_KeyDown;
			_textEditor.WordWrap = true;
			_textEditor.HorizontalScrollBarVisibility = Avalonia.Controls.Primitives.ScrollBarVisibility.Auto;
			_textEditor.Options.WordWrapIndentation = 10;
			_textEditor.Options.AllowScrollBelowDocument = true;
			_textEditor.IsVisible = true;

			_outputBox = this.FindControl<TextBox>("OutputBox");
			_outputBox.IsReadOnly = true;

			_runButton = this.FindControl<Button>("RunButton");
			_runButton.Click += runButton_ClickAsync;

			_stopButton = this.FindControl<Button>("StopButton");
			_stopButton.Click += stopButton_Click;
			_stopButton.IsVisible = false;

			_continueButton = this.FindControl<Button>("ContinueButton");
			_continueButton.Click += continueButton_ClickAsync;
			_continueButton.IsVisible = false;

			_inputBox = this.FindControl<TextBox>("InputBox");
			_inputBox.Text = "";

			_compiledBox = this.FindControl<TextBlock>("CompiledBox");
			_compiledBox.Background = Brushes.GhostWhite;
			_compiledBox.TextWrapping = TextWrapping.Wrap;

			_errorsBox = this.FindControl<TextBlock>("ErrorsBox");
			_errorsBox.Background = Brushes.GhostWhite;
			_errorsBox.TextWrapping = TextWrapping.Wrap;

			_dialogService = new DefaultDialogService();

			_newMenuItem = this.FindControl<MenuItem>("NewMenuItem");
			_newMenuItem.Click += _newMenuItem_ClickAsync;

			_openMenuItem = this.FindControl<MenuItem>("OpenMenuItem");
			_openMenuItem.Click += _openMenuItem_ClickAsync;

			_saveMenuItem = this.FindControl<MenuItem>("SaveMenuItem");
			_saveMenuItem.Click += _saveMenuItem_ClickAsync;

			_saveAsMenuItem = this.FindControl<MenuItem>("SaveAsMenuItem");
			_saveAsMenuItem.Click += _saveAsMenuItem_ClickAsync;

			_listOfFiles = this.FindControl<ListBox>("ListOfFiles");
			_listOfFiles.Items = _listOfFilesItems;

			_helpBox = this.FindControl<TextBlock>("HelpBox");
			//_helpBox.Text = File.ReadAllText("Help.txt");

			_helpMenuItem = this.FindControl<MenuItem>("HelpMenuItem");
			_helpMenuItem.Click += _helpMenuItem_Click;

			_helpCloseButton = this.FindControl<Button>("HelpCloseButton");
			_helpCloseButton.Click += _helpCloseButton_Click;

			_mainDockPanel = this.FindControl<DockPanel>("MainDockPanel");
			_mainDockPanel.Background = Brushes.DarkGray;

			_helpDockPanel = this.FindControl<DockPanel>("HelpDockPanel");

			_mainMenu = this.FindControl<Menu>("MainMenu");
			_mainMenu.Background = Brushes.LightGray;

		}

		private void _helpCloseButton_Click(object sender, RoutedEventArgs e) {
			_mainDockPanel.IsVisible = true;
			_helpDockPanel.IsVisible = false;
		}

		private void _helpMenuItem_Click(object sender, RoutedEventArgs e) {
			_mainDockPanel.IsVisible = false;
			_helpDockPanel.IsVisible = true;
		}

		private async void _newMenuItem_ClickAsync(object sender, RoutedEventArgs e) {
			await Save();
			_textEditor.Text = "";
			_compiledBox.Text = "";
			_outputBox.Text = "";
			_inputBox.Text = "";
			await SaveAs();
		}

		private async void _openMenuItem_ClickAsync(object sender, RoutedEventArgs e) {
			var result = await _dialogService.OpenFileDialogAsync();
			if (result == null) {
				return;
			}
			_textEditor.Text = File.ReadAllText(result[0]);
			CreateNewRecordInListOfFiles(result[0]);
		}

		private void runButton_ClickAsync(object sender, RoutedEventArgs e) {
			_compiledBox.Text = "";
			RunInterpretation();
		}

		private void stopButton_Click(object sender, RoutedEventArgs e) {
			_counter = 0;
			_runButton.IsVisible = true;
			_continueButton.IsVisible = false;
			_stopButton.IsVisible = false;
		}

		private void continueButton_ClickAsync(object sender, RoutedEventArgs e) {
			_isInterupted = true;
			RunInterpretation();
		}

		private async void _saveMenuItem_ClickAsync(object sender, RoutedEventArgs e) {
			await Save();
		}

		private async void _saveAsMenuItem_ClickAsync(object sender, RoutedEventArgs e) {
			await SaveAs();
		}

		private void _textEditor_TextChanged(object sender, EventArgs e) {
			if (_currentFile != null) {
				_currentFile.Value = false;
			}
		}

		private async void listBoxItem_TappedAsync(object sender, RoutedEventArgs e) {
			await Save();
			var senderRef = sender as ListBoxItem;
			string filePath = senderRef.DataContext as string;
			var file = File.OpenText(filePath);
			_textEditor.Text = file.ReadToEnd();
			file.Close();
			_currentFile = _currentFiles.Find(x => x.Key == filePath);
			_compiledBox.Text = "";
			_outputBox.Text = "";
			_inputBox.Text = "";
		}

		private async void listBoxItem_KeyDownAsync(object sender, KeyEventArgs e) {
			var senderRef = sender as ListBoxItem;
			string filePath = senderRef.DataContext as string;
			if (e.Key == Key.Delete || e.Key == Key.Back) {
				if (filePath == _currentFile.Key) {
					await Save();
					_textEditor.Text = "";
					_currentFile = null;
				}
				_listOfFilesItems.Remove(senderRef);
				Pair<string, bool> tempPair = new Pair<string, bool>(filePath, true);
				_currentFiles.Remove(tempPair);
			}
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TuringMachineCompiler.Compiler
{
    public class ErrorsHandler
    {
		public string Message {
			get; private set;
		}

		public bool WasError {
			get; private set;
		}

		public ErrorsHandler()
		{
			Message = "";
		}

		public void NewSyntaxError(string str, int i, string descr)
		{
			WasError = true;
			Message += $"Синтаксическая ошибка в строке {i} ({str}): {descr}\n";
			Console.WriteLine($"Синтаксическая ошибка в строке {i} ({str}): {descr}\n");
		}

		public void NewSystemError(string str)
		{
			WasError = true;
			Message += $"Системная ошибка: {str}\n";
			Console.WriteLine($"Системная ошибка: {str}");
		}

		public void NewIntrepretatorError(string str)
		{
			WasError = true;
			Message += $"Ошибка при интерпретации: {str}\n";
			Console.WriteLine($"Ошибка при интерпретации: {str}");
		}

		public void Add(ErrorsHandler errorsHandler)
		{
			Message += errorsHandler.Message;
			WasError = errorsHandler.WasError;
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Avalonia.Input;
using Avalonia.Media.Imaging;
using AvaloniaEdit;
using AvaloniaEdit.CodeCompletion;
using AvaloniaEdit.Document;
using AvaloniaEdit.Editing;

namespace Editor
{
    public partial class MainWindow
    {

		private List<(string, string)> LocalStates {
			get {
				List<(string, string)> list = new List<(string, string)>();
				foreach (var line in _textEditor.Text.Split('\n')) {
					if (line.Contains("#")) {
						continue;
					}
					line.Trim(' ', '\t', '\r');
					if (line.Contains(" ")) {
						var state = line.Substring(0, line.IndexOf(' '));
						if (!list.Contains( (state, "") )) {
							list.Add( (line.Substring(0, line.IndexOf(' ')), "") );
						}
					}
				}
				list.Sort();
				return list;
			}
		}

		private List<string> LocalStatesS {
			get {
				List<string> list = new List<string>();
				foreach (var line in _textEditor.Text.Split('\n')) {
					if (line.Contains("#")) {
						continue;
					}
					line.Trim(' ', '\t', '\r');
					if (line.Contains(" ")) {
						var state = line.Substring(0, line.IndexOf(' '));
						if (!list.Contains(state)) {
							list.Add(line.Substring(0, line.IndexOf(' ')));
						}
					}
				}
				list.Sort();
				return list;
			}
		}

		private void ShowCompletionList(IEnumerable<string> datas)
		{
			_completionWindow = new CompletionWindow(_textEditor.TextArea);
			_completionWindow.Closed += (o, args) => _completionWindow = null;
			foreach (var item in datas) {
				var mcd = new MyCompletionData(item);
				_completionWindow.CompletionList.CompletionData.Add(mcd);
				_completionWindow.Width = 200;
			}
			_completionWindow.Show();
		}

		private void ShowInsightWindow(IList<(string, string)> datas)
		{
			_insightWindow = new OverloadInsightWindow(_textEditor.TextArea);
			_insightWindow.Closed += (o, args) => _insightWindow = null;
			_insightWindow.Provider = new MyOverloadProvider(datas);
			_insightWindow.Show();
		}

		private CompletionWindow _completionWindow;
		private OverloadInsightWindow _insightWindow;

		void textEditor_TextArea_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.LeftCtrl) {
				ShowCompletionList(LocalStatesS);
			}
			if (e.Key == Key.LeftShift) {
				ShowInsightWindow(LocalStates);
				e.Handled = true;
			}
		}

		private class MyOverloadProvider : IOverloadProvider
		{
			private readonly IList<(string header, string content)> _items;
			private int _selectedIndex;

			public MyOverloadProvider(IList<(string header, string content)> items)
			{
				_items = items;
				SelectedIndex = 0;
			}

			public int SelectedIndex {
				get => _selectedIndex;
				set {
					_selectedIndex = value;
					OnPropertyChanged();
					// ReSharper disable ExplicitCallerInfoArgument
					OnPropertyChanged(nameof(CurrentHeader));
					OnPropertyChanged(nameof(CurrentContent));
					// ReSharper restore ExplicitCallerInfoArgument
				}
			}

			public int Count => _items.Count;
			public string CurrentIndexText => null;
			public object CurrentHeader => _items[SelectedIndex].header;
			public object CurrentContent => _items[SelectedIndex].content;

			public event PropertyChangedEventHandler PropertyChanged;

			private void OnPropertyChanged([CallerMemberName] string propertyName = null)
			{
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public class MyCompletionData : ICompletionData
		{
			public MyCompletionData(string text)
			{
				Text = text;
				Description = text;
			}

			public MyCompletionData(string text, string description)
			{
				Text = text;
				Description = description;
			}

			public IBitmap Image => null;

			public string Text { get; }

			// Use this property if you want to show a fancy UIElement in the list.
			public object Content => Text;

			public object Description {
				get;
			}

			public double Priority { get; } = 20;

			public void Complete(TextArea textArea, ISegment completionSegment,
				EventArgs insertionRequestEventArgs)
			{
				textArea.Document.Replace(completionSegment, Text);
			}

		}
	}
}

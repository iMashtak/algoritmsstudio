﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TuringMachineCompiler.Compiler
{
    class SyntaxAnalyser
    {
		public ErrorsHandler ErrorsHandler {
			get; private set;
		}

		private IEnumerable<string> codelines;

		private static readonly string[] keywords = new string[] {
			"include",
			"namespace"
		};

		private HashSet<string> statesInCode;

		private HashSet<char> symbolsInCode;

		private HashSet<string> includedFiles;

		public SyntaxAnalyser(IEnumerable<string> codelines)
		{
			this.codelines = codelines;
			statesInCode = new HashSet<string>();
			symbolsInCode = new HashSet<char>();
			includedFiles = new HashSet<string>();
		}

		public void Analyse()
		{
			ErrorsHandler = new ErrorsHandler();
			int lineNumber = 0;
			foreach (var line in codelines) {
				lineNumber += 1;
				if (line.IsFirstSymbolEquals('#')) {
					var answer = AnalyseDirective(line);
					if (answer.result == false) {
						ErrorsHandler.NewSyntaxError(line, lineNumber, answer.description);
					}
					continue;
				}
				var parts = line.Split(' ');
				if (parts.Length != 6) {
					ErrorsHandler.NewSyntaxError(line, lineNumber, "Неверное число операндов в строке");
					continue;
				}
				string currentState = parts[0];
				string foundedSymbol = parts[1];
				string newSymbol = parts[3];
				string direction = parts[4];
				string newState = parts[5];
				if (parts[2] != "->") {
					ErrorsHandler.NewSyntaxError(line, lineNumber, "В каждой строке необходимо проводить " +
						"операцию преобразования");
					continue;
				}
				statesInCode.Add(currentState);
				statesInCode.Add(newState);
				foreach (var symbol in foundedSymbol + newSymbol) {
					symbolsInCode.Add(symbol);
				}
				if (direction != "R" && direction != "H" && direction != "L") {
					ErrorsHandler.NewSyntaxError(line, lineNumber, "Недопустимый вариант движения. Возможные: L, H, R");
				}
			}
			if (namespaceFlag == false) {
				ErrorsHandler.NewSyntaxError("", -1, "Необходимо указать пространство имён, которому принадлежит файл");
			}
		}

		public string[] GetCodeLines()
		{
			return codelines.ToArray();
		}

		public string[] GetStates()
		{
			return statesInCode.ToArray();
		}

		public char[] GetSymbols()
		{
			return symbolsInCode.ToArray();
		}

		public string[] GetPathOfIncludedFiles()
		{
			return includedFiles.ToArray();
		}

		private static bool namespaceFlag = false;
		private (bool result, string description) AnalyseDirective(string line)
		{
			var word = line.GetWordAfterSymbol('#');
			if (keywords.Contains(word)) {
				if (word == "include") {
					AnalyseInclude(line);
				}
				if (word == "namespace") {
					if (namespaceFlag == true) {
						return (false, "В файле может содержаться только один namespace");
					}
					namespaceFlag = true;
					var parts = line.Split(' ');
					if (parts.Length != 2) {
						return (false, "У директивы namespace может быть только один аргумент");
					}
				}
				return (true, "");
			}
			return (false, "Неизвестное ключевое слово");
		}

		private void AnalyseInclude(string line)
		{
			int index = line.IndexOf("#include");
			index += "#include".Length + 1;
			string path = line.Substring(index);
			includedFiles.Add(path);
		}

		public static bool ContainsAnyKeyword(string line)
		{
			foreach (var keyword in keywords) {
				if (line.Contains($"#{keyword}")) {
					return true;
				}
			}
			return false;
		}

    }

}

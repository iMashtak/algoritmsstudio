﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TuringMachineCompiler.Compiler
{
    static class StringExtensions
    {
		public static bool IsFirstSymbolEquals(this string line, char symbol)
		{
			return (from smbl in line where smbl != ' ' && smbl != '\t' select smbl == symbol).FirstOrDefault();
		}

		public static string RemoveSpacesFromBegining(this string line)
		{
			for (int i = 0; i < line.Length; ++i) {
				if (line[i] != ' ' || line[i] != '\t') {
					return line.Substring(i);
				}
			}
			return "";
		}

		public static string GetWordAfterSymbol(this string line, char in_symbol)
		{
			// не производит проверок на содержание в строке пробела и требуемого символа
			int indexOfSymbol = line.IndexOf(in_symbol);
			int endIndex = 0;
			for (int i = indexOfSymbol; i < line.Length; ++i) {
				if (line[i] == ' ') {
					endIndex = i;
					break;
				}
			}
			return line.Substring(indexOfSymbol + 1, endIndex - indexOfSymbol - 1);
		}
	}
}

﻿using System;
using System.IO;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Platform;
using System.Threading.Tasks;

namespace Editor
{
    public partial class MainWindow : Window
    {
        const string TuringType = ".tm";
        const string MarkovType = ".mm";

        private string CurrentType => GetFileType(_currentFile.Key);

        public MainWindow()
        {
            InitializeComponent();
            this.AttachDevTools();
        }

        private void CreateNewRecordInListOfFiles(string path)
        {
            _currentFiles.Add(new Pair<string, bool>(path, true));
            _currentFile = _currentFiles[^1];
            var item = new ListBoxItem
            {
                Content = GetFileName(_currentFile.Key),
                DataContext = _currentFile.Key
            };
            item.Tapped += listBoxItem_TappedAsync;
            item.KeyDown += listBoxItem_KeyDownAsync;
            _listOfFilesItems.Add(item);
        }

        private async Task Save()
        {
            if (_currentFile == null)
            {
                if (_textEditor.Text != "")
                {
                    await SaveAs();
                }

                return;
            }

            if (_currentFile.Value == true)
            {
                return;
            }

            File.WriteAllText(_currentFile.Key, _textEditor.Text);
        }

        private async Task SaveAs()
        {
            var result = await _dialogService.SaveFileDialogAsync();
            if (result == null)
            {
                return;
            }

            StreamWriter writer = File.CreateText(result);
            writer.Write(_textEditor.Text);
            writer.Close();
            CreateNewRecordInListOfFiles(result);
        }

        int _counter = 0;
        string _savedInputText = "";
        bool _isInterupted = false;

        private void RunInterpretation()
        {
            if (_isInterupted == false)
            {
                _counter = 0;
                _compiledBox.Text = "";
            }

            if (CurrentType == TuringType)
            {
                StartTuringMachine();
            }

            if (CurrentType == MarkovType)
            {
                StartMarkovMashine();
            }
        }

        private string GetFileName(string filePath)
        {
            var version = Environment.OSVersion;
            string temp = "";
            if (version.Platform == PlatformID.Win32NT)
            {
                temp = filePath.Substring(filePath.LastIndexOf('\\') + 1);
            }

            if (version.Platform == PlatformID.Unix)
            {
                temp = filePath.Substring(filePath.LastIndexOf('/') + 1);
            }

            return temp;
        }

        private string GetFileNameNoType(string filePath)
        {
            var temp = GetFileName(filePath);
            return temp.Remove(temp.LastIndexOf('.'));
        }

        private string GetFileType(string filePath)
        {
            return filePath.Substring(filePath.LastIndexOf('.'));
        }
    }
}
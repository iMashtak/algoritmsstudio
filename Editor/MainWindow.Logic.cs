﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using TuringMachineCompiler.Compiler;

namespace Editor
{
    public partial class MainWindow
    {
        private void StartMarkovMashine()
        {
            int countofsteps = 10;
            string inputText = _isInterupted ? _savedInputText : _inputBox.Text;
            string inputCodeText = _textEditor.Text;
            string[] codeLines = inputCodeText.Split('\n');
            int local_counter = 0;
            for (int i = 0; i < codeLines.Length; ++i)
            {
                if (codeLines[i] == "")
                {
                    continue;
                }

                if (codeLines[i][codeLines[i].Length - 1] == '\r')
                {
                    codeLines[i] = codeLines[i].Remove(codeLines[i].Length - 1);
                }

                string[] subStrs = codeLines[i].Split('-');
                if (subStrs.Length != 2)
                {
                    _outputBox.Text = "Interpretation error";
                    return;
                }

                bool flag_point = codeLines[i].Contains(".");
                if (flag_point)
                {
                    subStrs[1] = subStrs[1].Remove(subStrs[1].IndexOf('.'));
                }

                if (inputText.Contains(subStrs[0]))
                {
                    ++local_counter;
                    _compiledBox.Text += $"{++_counter}: line {i + 1} -> {codeLines[i]}";
                    i = -1;
                    var index = inputText.IndexOf(subStrs[0]);
                    inputText = inputText.Remove(index, subStrs[0].Length);
                    inputText = inputText.Insert(index, subStrs[1]);
                    _compiledBox.Text = _compiledBox.Text + $" : {inputText}\n";
                    if (flag_point)
                    {
                        break;
                    }
                }

                if (local_counter > codeLines.Length * countofsteps * (_inputBox.Text.Length + 1))
                {
                    _outputBox.Text = "Interupted";
                    _stopButton.IsVisible = true;
                    _continueButton.IsVisible = true;
                    _runButton.IsVisible = false;
                    _savedInputText = inputText;
                    return;
                }
            }

            _outputBox.Text = inputText;
            _isInterupted = false;
        }

        private Interpretator _interpretator;

        private void StartTuringMachine()
        {
            var inputText = _isInterupted ? _savedInputText : _inputBox.Text;
            _interpretator = new Interpretator(Interpretator.Mode.Debug);
            _interpretator.Printer = delegate(string message) { _compiledBox.Text += message; };
            if (_interpretator.Interpretate(inputText, _currentFile.Key, out var result) == false)
            {
                _outputBox.Text = "";
                _compiledBox.Text = "";
                _errorsBox.Text = _interpretator.ErrorsHandler.Message;
                return;
            }

            _outputBox.Text = result;
        }
    }
}
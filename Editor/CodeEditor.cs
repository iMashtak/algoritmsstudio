﻿using System;
using System.Collections.Generic;
using System.Text;
using Avalonia.Input;
using Avalonia.Media.Imaging;
using AvaloniaEdit;
using AvaloniaEdit.CodeCompletion;
using AvaloniaEdit.Document;
using AvaloniaEdit.Editing;

namespace Editor
{
    class CodeEditor : TextEditor
    {
		public CompletionWindow CompletionWindow = null;
		public CodeEditor()
		{
			this.TextArea.KeyDown += TextArea_KeyDown;
		}

		void TextArea_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.LeftCtrl) {
				ShowCompletion("");
			}
		}

		private void ShowCompletion(string enteredText)
		{
			CompletionWindow = new CompletionWindow(TextArea);
			IList<ICompletionData> data = CompletionWindow.CompletionList.CompletionData;
			data.Add(new MyCompletionData("AAAAAAAAAAAA"));
			CompletionWindow.Show();
			CompletionWindow.Closed += delegate
			{
				CompletionWindow = null;
			};
		}
	}

	public class MyCompletionData : ICompletionData
	{
		public MyCompletionData(string text)
		{
			Text = text;
			Description = text;
		}

		public MyCompletionData(string text, string description)
		{
			Text = text;
			Description = description;
		}

		public IBitmap Image => null;

		public string Text {
			get;
		}

		// Use this property if you want to show a fancy UIElement in the list.
		public object Content => Text;

		public object Description {
			get;
		}

		public double Priority { get; } = 20;

		public void Complete(TextArea textArea, ISegment completionSegment,
			EventArgs insertionRequestEventArgs)
		{
			textArea.Document.Replace(completionSegment, Text);
		}

	}
}

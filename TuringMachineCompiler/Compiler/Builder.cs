﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;

namespace TuringMachineCompiler.Compiler
{
    public class Builder
    {
		public ErrorsHandler ErrorsHandler {
			get; private set;
		}

		public string MainNamespace {
			get; private set;
		}

		private List<string> searchedPaths = new List<string>();

		public Builder()
		{
		}

		private void RenameStatesWithNamespaces(List<string> codeLines)
		{
			string mNamespaceLine = codeLines.Find(x => x.Contains("#namespace"));
			var mNamespace = mNamespaceLine.Split(' ')[1];
			if (MainNamespace == null) {
				MainNamespace = mNamespace;
			}
			for (int j = 0; j < codeLines.Count; ++j) {
				if (SyntaxAnalyser.ContainsAnyKeyword(codeLines[j])) {
					continue;
				}
				var parts = codeLines[j].Split(' ');
				if (parts[0].Contains('_') == false) {
					parts[0] = mNamespace + "_" + parts[0];
				}
				if (parts[5].Contains('_') == false) {
					parts[5] = mNamespace + "_" + parts[5];
				}
				codeLines[j] = $"{parts[0]} {parts[1]} {parts[2]} {parts[3]} {parts[4]} {parts[5]}";
			}
		}

		private List<string> TakeAllIncludedCode(string codePath) {
			if (File.Exists(codePath) == false) {
				ErrorsHandler.NewSystemError($"Не существует файла по пути: {codePath}");
				return null;
			}
			string codeString = File.ReadAllText(codePath);
			List<string> codeLines = Parser.Parse(codeString);
			SyntaxAnalyser syntaxAnalyser = new SyntaxAnalyser(codeLines);
			syntaxAnalyser.Analyse();
			ErrorsHandler.Add(syntaxAnalyser.ErrorsHandler);
			if (syntaxAnalyser.ErrorsHandler.WasError) {
				ErrorsHandler.NewSystemError($"Найдены ошибки в файле: {codePath}");
				return null;
			}
			RenameStatesWithNamespaces(codeLines);
			foreach (var path in syntaxAnalyser.GetPathOfIncludedFiles()) {
				codeLines.RemoveAll(x => x.Contains($"#include {path}"));
				if (File.Exists(path)) {
					var additionalCode = File.ReadAllText(path);
					if (searchedPaths.Contains(path)) {
						continue;
					}
					searchedPaths.Add(path);
					var additionalCodeLines = this.TakeAllIncludedCode(additionalCode);
					if (additionalCodeLines == null) {
						return null;
					}
					foreach (var line in additionalCodeLines) {
						codeLines.Add(line);
					}
				}
			}
			return codeLines;
		}

		public bool Build(string codePath, out string[] result)
		{
			ErrorsHandler = new ErrorsHandler();
			MainNamespace = null;
			var resultingCode = TakeAllIncludedCode(codePath);
			if (resultingCode == null) {
				result = null;
				return false;
			}
			result = resultingCode.ToArray();
			return true;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Avalonia.Controls;

namespace Editor {
	public class DefaultDialogService {

		public async Task<string[]> OpenFileDialogAsync() {
			var openFileDialog = new OpenFileDialog();
			var fDF_MT = new FileDialogFilter {
				Name = "Turing mashine",
				Extensions = new List<string> {
				"tm"
			}
			};
			var fDF_MM = new FileDialogFilter {
				Name = "Markov mashine",
				Extensions = new List<string> {
				"mm"
			}
			};
			openFileDialog.Filters.Add(fDF_MT);
			openFileDialog.Filters.Add(fDF_MM);
			var result = await openFileDialog.ShowAsync(new Window());
			return result;
		}

		public async Task<string> SaveFileDialogAsync() {
			var saveFileDialog = new SaveFileDialog();
			var fDF_MT = new FileDialogFilter {
				Name = "Turing machine",
				Extensions = new List<string> {
				"tm"
			}
			};
			var fDF_MM = new FileDialogFilter {
				Name = "Markov machine",
				Extensions = new List<string> {
				"mm"
			}
			};
			saveFileDialog.Filters.Add(fDF_MT);
			saveFileDialog.Filters.Add(fDF_MM);
			var result = await saveFileDialog.ShowAsync(new Window());
			return result;
		}

	}
}
